// Lim Ngian Xin Terry
// A0218430N

import java.util.*;
import java.io.*;

public class AVLTree<T> {
    private class Node {
        public T value;
        public int count;
        public int totalCount;
        public int height;
        public Node parent, left, right;

        public Node(T value, int count) {
            this.value = value;
            this.count = this.totalCount = count;
            this.height = 0;
            this.parent = this.left = this.right = null;
        }
    }

    Node root;
    Comparator<? super T> comparator;

    private boolean isRoot(Node node) { return node.parent != null; }
    private boolean isLeaf(Node node) { return node.left == null && node.right == null; }
    private boolean isInternal(Node node) { return !isRoot(node) && !isLeaf(node); }
    private boolean hasLeftChild(Node node) { return node.left != null; }
    private boolean hasRightChild(Node node) { return node.right != null; }
    private boolean hasBothChildren(Node node) { return hasLeftChild(node) && hasRightChild(node); }
    private boolean isLeftChild(Node node) { return !isRoot(node) && node == node.parent.left; }
    private boolean isRightChild(Node node) { return !isRoot(node) && node == node.parent.right; }

    private void updateTotalCount(Node node) {
        if (node == null) { return; }
        int leftTotalCount = 0;
        int rightTotalCount = 0;

        if (hasLeftChild(node)) {
            leftTotalCount = node.left.totalCount;
        }
        if (hasRightChild(node)) {
            rightTotalCount = node.right.totalCount;
        }
        node.totalCount = leftTotalCount + rightTotalCount + node.count;
    }

    private void updateHeight(Node node) {
        if (node == null) { return; }
        int leftHeight = -1;
        int rightHeight = -1;

        if (hasLeftChild(node)) {
            leftHeight = node.left.height;
        }
        if (hasRightChild(node)) {
            rightHeight = node.right.height;
        }
        node.height = Math.max(leftHeight, rightHeight) + 1;
    }

    private int balanceFactor(Node node) {
        int leftHeight = -1;
        int rightHeight = -1;

        if (hasLeftChild(node)) {
            leftHeight = node.left.height;
        }
        if (hasRightChild(node)) {
            rightHeight = node.right.height;
        }
        return leftHeight - rightHeight;
    }

    private Node findMin(Node node) {
        if (node == null) { return null; }
        if (node.left == null) { return node; }
        return findMin(node.left);
    }

    private Node findMax(Node node) {
        if (node == null) { return null; }
        if (node.right == null) { return node; }
        return findMax(node.right);
    }

    private Node findPredecessor(Node node) {
        if (node == null) { return null; }

        // If there is a left subtree, search for predecessor there.
        if (hasLeftChild(node)) {
            return findMax(node.left);
        }

        // Else, search upstream.
        Node child = node;
        Node parent = node.parent;
        while (isLeftChild(child)) {
            child = parent;
            parent = parent.parent;
        }
        return parent;
    }

    private Node findSuccessor(Node node) {
        if (node == null) { return null; }

        // If there is a right subtree, search for successor there.
        if (hasRightChild(node)) {
            return findMin(node.right);
        }

        // Else, search upstream.
        Node child = node;
        Node parent = node.parent;
        while (isRightChild(child)) {
            child = parent;
            parent = parent.parent;
        }
        return parent;
    }

    private Node rotateLeft(Node node) {
        if (node == null) { return null; }

        /*
         * A---------                          B---------
         * |        |      Rotate Right ->     |        |
         * B----    C      <- Rotate Left      D    ----A
         * |   |                                    |   |
         * D   E                                    E   C
        */

        Node b = node;
        Node a = b.right;
        Node e = a.left;

        a.left = b;
        a.parent = b.parent;

        b.parent = a;
        b.right = e;
        
        if (e != null) { e.parent = b; }

        updateHeight(b);
        updateTotalCount(b);
        updateHeight(a);
        updateTotalCount(a);

        return a;
    }

    private Node rotateRight(Node node) {
        if (node == null) { return null; }

        /*
         * A---------                          B---------
         * |        |      Rotate Right ->     |        |
         * B----    C      <- Rotate Left      D    ----A
         * |   |                                    |   |
         * D   E                                    E   C
        */

        Node a = node;
        Node b = a.left;
        Node e = b.right;

        b.right = a;
        b.parent = a.parent;

        a.parent = b;
        a.left = e;

        if (e != null) { e.parent = a; }

        updateHeight(a);
        updateTotalCount(a);
        updateHeight(b);
        updateTotalCount(b);

        return b;
    }

    private Node balance(Node node) {
        if (node == null) { return node; }

        // Left-X Case
        if (balanceFactor(node) == 2) {
            // Left-Right Case
            if (balanceFactor(node.left) == -1) {
                node.left = rotateLeft(node.left);
                return rotateRight(node);
            }
            // Left-Left Case
            if (balanceFactor(node.left) == 1) {
                return rotateRight(node);
            }
        }

        // Right-X Case
        if (balanceFactor(node) == -2) {
            // Right-Left Case
            if (balanceFactor(node.right) == 1) {
                node.right = rotateRight(node.right);
                return rotateLeft(node);
            }
            // Right-Right Case
            if (balanceFactor(node.right) == -1) {
                return rotateLeft(node);
            }
        }

        return node;
    }

    private Node insert(Node node, T value, int count) {
        if (node == null) { return new Node(value, count); }

        int c = comparator.compare(node.value, value);

        // Insert right.
        if (c > 0) {
            node.right = insert(node.right, value, count);
            node.right.parent = node;
            updateHeight(node);
            updateTotalCount(node);
            node = balance(node);
        }
        // Insert left.
        else if (c < 0) {
            node.left = insert(node.left, value, count);
            node.left.parent = node;
            updateHeight(node);
            updateTotalCount(node);
            node = balance(node);
        } else {
            // Insert here.
            node.count += count;
            updateTotalCount(node);
        }

        return node;
    }

    private Node remove(Node node, T value, int count) {
        if (node == null) { return null; }

        int c = comparator.compare(node.value, value);

        // Search to the right.
        if (c > 0) {
            node.right = remove(node.right, value, count);
            updateHeight(node);
            updateTotalCount(node);
            node = balance(node);
            return node;
        }
        // Search to the left.
        else if (c < 0) {
            node.left = remove(node.left, value, count);
            updateHeight(node);
            updateTotalCount(node);
            node = balance(node);
            return node;
        }

        // Remove this node.
        node.count -= count;
        updateTotalCount(node);

        if (node.count > 0) { // Decrease count but do not remove node.
            return node;
        }
        if (hasBothChildren(node)) { // Has both children.
            Node successor = findMin(node.right);
            node.value = successor.value;
            node.count = successor.count;
            node.right = remove(node.right, successor.value, successor.count);
            
            updateHeight(node);
            updateTotalCount(node);
            return node;
        }
        if (hasRightChild(node)) { // Only has right child.
            node.right.parent = node.parent;
            return node.right;
        }
        if (hasLeftChild(node)) { // Only has left child.
            node.left.parent = node.parent;
            return node.left;
        }

        return null; // This node is a leaf.
    }

    private int getNumGreater(Node node, T value) {
        if (node == null) { return 0; }

        int c = comparator.compare(node.value, value);
        int rightTotalCount = hasRightChild(node) ? node.right.totalCount : 0;

        if (c == 0) {
            return rightTotalCount;
        }
        if (c > 0) {
            return getNumGreater(node.right, value);
        }
        return node.count + rightTotalCount + getNumGreater(node.left, value);
    }

    private int getHeight(Node node) {
        return node == null ? -1 : node.height;
    }

    public AVLTree(Comparator<? super T> comparator) {
        this.root = null;
        this.comparator = comparator;
    }
    
    public void insert(T value, int count) { root = insert(root, value, count); }
    public void remove(T value, int count) { root = remove(root, value, count); }
    public int getNumGreater(T value) { return getNumGreater(root, value); }
    public int getHeight() { return getHeight(root); }
}