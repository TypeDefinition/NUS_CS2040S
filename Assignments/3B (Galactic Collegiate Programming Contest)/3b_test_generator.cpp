// By Lim Ngian Xin Terry

#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <stdexcept>
#include <fstream>
#include <unordered_set>

int main(int argc, char *argv[])
{
    const int min = 1;
    const int max = 100000;

    // Read input.
    const int n = std::atoi(argv[1]);
    const int m = std::atoi(argv[2]);
    const std::string in_filename = std::string(argv[3]);
    const std::string out_filename = std::string(argv[4]);
    if (n < min || n > max || m < min || m > max)
    {
        throw std::invalid_argument("Invalid input. Arguments must be 2 integers between " + std::to_string(min) + " to " + std::to_string(max) + " inclusive, followed by the input and output filenames.\nExample: $ ./3b_test_generator 54 722 MyRandomTest.in MyRandomTest.out");
    }

    std::srand(std::time(nullptr));
    int t_array[max] = {0};
    long p_array[max] = {0};
    int results[max] = {0};

    // Generator random cases.
    for (int i = 0; i < m; ++i)
    {
        t_array[i] = rand() % n + 1;
        p_array[i] = static_cast<long>(rand() % 1000 + 1);
    }

    // Run simulation.
    std::unordered_set<int> set;
    int points[max] = {0};
    long penalties[max] = {0};
    for (int i = 0; i < m; ++i)
    {
        int t = t_array[i] - 1;
        long p = p_array[i];

        ++points[t];
        penalties[t] += p;

        if (t == 0)
        {
            std::unordered_set<int>::iterator iter = set.begin();
            while (iter != set.end())
            {
                if (points[*iter] < points[0] || (points[*iter] == points[0] && penalties[*iter] >= penalties[0]))
                {
                    iter = set.erase(iter);
                    continue;
                }
                ++iter;
            }
        }
        else
        {
            if (points[t] > points[0] || (points[t] == points[0] && penalties[t] < penalties[0]))
            {
                set.insert(t);
            }
        }

        results[i] = set.size() + 1;
    }

    // Generator input file.
    std::ofstream in_file(in_filename, std::ofstream::trunc);
    if (!in_file.is_open())
    {
        throw std::invalid_argument("Unable to open input file.");
    }
    in_file << std::to_string(n) << " " << std::to_string(m) << "\n";
    for (int i = 0; i < m; ++i)
    {
        in_file << std::to_string(t_array[i]) << " " << std::to_string(p_array[i]) << "\n";
    }
    in_file.close();

    // Generate output file.
    std::ofstream out_file(out_filename, std::ofstream::trunc);
    if (!out_file.is_open())
    {
        throw std::invalid_argument("Unable to open output file.");
    }
    for (int i = 0; i < m; ++i)
    {
        out_file << std::to_string(results[i]) << "\n";
    }

    out_file.close();

    return 0;
}