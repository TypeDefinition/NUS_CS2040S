// Lim Ngian Xin Terry
// A0218430N

import java.util.*;
import java.io.*;

class ContestRank implements Comparable<ContestRank> {
    public final int points;
    public final long penalties;

    public ContestRank(int points, long penalties) {
        this.points = points;
        this.penalties = penalties;
    }

    public int compareTo(ContestRank other) {
        if (this.points == other.points) {
            if (this.penalties < other.penalties) {
                return -1;
            }
            if (this.penalties > other.penalties) {
                return 1;
            }
            return 0;
        }
        return other.points - this.points;
    }

    public String toString() {
        return "(" + Integer.toString(points) + ", " + Long.toString(penalties) + ")";
    }
}

public class Asn3B {
    public static void main(String _args[]) throws Exception {
        // Initialise IO.
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter printWriter = new PrintWriter(System.out);

        // Read N & M.
        String firstLine[] = bufferedReader.readLine().split(" ");
        int n = Integer.parseInt(firstLine[0]);
        int m = Integer.parseInt(firstLine[1]);

        AVLTree<ContestRank> tree = new AVLTree<>(ContestRank::compareTo);
        tree.insert(new ContestRank(0, 0), n);
        int points[] = new int[n];
        long penalties[] = new long[n];
        for (int i = 0; i < m; ++i) {
            String line[] = bufferedReader.readLine().split(" ");
            int t = Integer.parseInt(line[0]) - 1;
            long p = Long.parseLong(line[1]);

            tree.remove(new ContestRank(points[t], penalties[t]), 1);
            tree.insert(new ContestRank(++points[t], penalties[t] += p), 1);
            printWriter.println(tree.getNumGreater(new ContestRank(points[0], penalties[0])) + 1);
        }

        // Close IO.
        bufferedReader.close();
        printWriter.close();
    }
}