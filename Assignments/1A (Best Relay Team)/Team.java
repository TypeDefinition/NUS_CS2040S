// Lim Ngian Xin Terry
// A0218430N

import java.util.*;

public class Team {
    private Runner[] runners;
    private double lapTime;

    public Team(Runner[] runners) {
        // Assign Runners
        this.runners = runners;

        // Calculate Lap Time
        this.lapTime = 0.0f;
        this.lapTime += this.runners[0].leg1Time;
        for (int i = 1; i < this.runners.length; ++i) {
            this.lapTime += this.runners[i].legNTime;
        }
    }

    public double getLapTime() {
        return this.lapTime;
    }

    public Runner[] getRunners() {
        return runners;
    }
}