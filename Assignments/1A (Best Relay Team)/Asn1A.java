// Lim Ngian Xin Terry
// A0218430N

import java.util.*;

public class Asn1A {
    public static void main(String[] _args) {
        Scanner scanner = new Scanner(System.in);
        int numRunners = scanner.nextInt();
        scanner.nextLine();

        Runner[] nominalRoll = new Runner[numRunners];
        // Read in the runners.
        for (int i = 0; i < numRunners; ++i) {
            nominalRoll[i] = new Runner(scanner.next(), scanner.nextDouble(), scanner.nextDouble());
            if (!scanner.hasNextLine()) {
                break;
            }
            scanner.nextLine();
        }
        scanner.close();

        // Sort the runners by their Leg N timing.
        Arrays.parallelSort(nominalRoll,(Runner a, Runner b) -> {
            if (a.legNTime < b.legNTime) { return -1; }
            if (a.legNTime > b.legNTime) { return 1; }
            return 0;
        });

        // Form multiple teams.
        ArrayList<Team> teams = new ArrayList<Team>();
        for (int i = 0; i < nominalRoll.length; ++i) {
            Runner[] runners = new Runner[4];
            runners[0] = nominalRoll[i];

            int runnersIndex = 1;
            int nominalRollIndex = 0;
            while (runnersIndex < 4) {
                Runner runnerN = nominalRoll[nominalRollIndex++];
                if (runnerN != nominalRoll[i]) {
                    runners[runnersIndex++] = runnerN;
                }
            }
            teams.add(new Team(runners));
        }

        // Get the fastest team.
        Team fastestTeam = teams.get(0);
        for (int i = 1; i < teams.size(); i++) {
            Team team = teams.get(i);
            if (team.getLapTime() < fastestTeam.getLapTime()) {
                fastestTeam = team;
            }
        }
        
        System.out.println(fastestTeam.getLapTime());
        for (int i = 0; i < fastestTeam.getRunners().length; ++i) {
            System.out.println(fastestTeam.getRunners()[i].name);
        }
    }
}