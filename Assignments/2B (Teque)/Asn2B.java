// Lim Ngian Xin Terry
// A0218430N

import java.util.*;
import java.io.*;

class Teque {
    int frontHead;
    int frontTail;
    int frontSize;
    int frontList[];

    int backHead;
    int backTail;
    int backSize;
    int backList[];

    public Teque() {
        int size = 1000000;
        frontHead = size/2;
        frontTail = size/2;
        frontSize = 0;
        frontList = new int[size];

        backHead = size/2;
        backTail = size/2;
        backSize = 0;
        backList = new int[size];
    }

    public void pushFront(int value) {
        frontList[frontTail++] = value;
        ++frontSize;

        if (frontSize - backSize > 1) {
            backList[--backHead] = frontList[frontHead++];
            ++backSize;
            --frontSize;
        }
    }

    public void pushMiddle(int value) {
        frontList[--frontHead] = value;
        ++frontSize;

        if (frontSize - backSize > 1) {
            backList[--backHead] = frontList[frontHead++];
            ++backSize;
            --frontSize;
        }
    }

    public void pushBack(int value) {
        backList[backTail++] = value;
        ++backSize;

        if (backSize > frontSize) {
            frontList[--frontHead] = backList[backHead++];
            ++frontSize;
            --backSize;
        }
    }

    public int get(int index) {
        if (index < frontSize) {
            return frontList[frontTail - index - 1];
        } else {
            return backList[index - frontSize + backHead];
        }
    }
}

public class Asn2B {
    public static void main(String[] _args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter printWriter = new PrintWriter(System.out);

        Teque teque = new Teque();
        int numOps = Integer.parseInt(bufferedReader.readLine());
        for (int i = 0; i < numOps; ++i) {
            StringTokenizer tokenizer = new StringTokenizer(bufferedReader.readLine(), " ");
            String op = tokenizer.nextToken();
            int value = Integer.parseInt(tokenizer.nextToken());

            if (op.equals("push_front")) {
                teque.pushFront(value);
            } else if (op.equals("push_middle")) {
                teque.pushMiddle(value);
            } else if (op.equals("push_back")) {
                teque.pushBack(value);
            } else if (op.equals("get")) {
                printWriter.println(Integer.toString(teque.get(value)));
            }
        }

        bufferedReader.close();
        printWriter.close();
    }
}