// Lim Ngian Xin Terry
// A0218430N

import java.util.*;
import java.io.*;

class UFDS {
    private final int numElements;
    private final int rank[];
    private final int parents[];
    private final int sizeOfSet[];
    private final int numFilledDrawers[];

    public UFDS(int numElements) {
        this.numElements = numElements;
        this.rank = new int[numElements];
        this.parents = new int[numElements];
        this.sizeOfSet = new int[numElements];
        this.numFilledDrawers = new int[numElements];

        Arrays.fill(this.sizeOfSet, 1);
        for (int i = 0; i < numElements; ++i) {
            this.parents[i] = i;
        }
    }

    public int getNumElements() {
        return numElements;
    }

    public int findSet(int index) {
        return (parents[index] == index) ? index : (parents[index] = findSet(parents[index]));
    }

    public boolean isSameSet(int a, int b) {
        return findSet(a) == findSet(b);
    }

    // Union set A and set B and return the root.
    public int unionSet(int a, int b) {
        // Find the set that A and B belongs to.
        int aRoot = findSet(a);
        int bRoot = findSet(b);

        // Do nothing if both elements are already in the same set.
        if (aRoot == bRoot) {
            return aRoot;
        }

        // If rank[A] < rank[B], place A under B.
        if (rank[aRoot] < rank[bRoot]) {
            parents[aRoot] = bRoot;
            sizeOfSet[bRoot] += sizeOfSet[aRoot];

            // A set of size N represents N drawers. In this set, how many of the drawers
            // are filled?
            numFilledDrawers[bRoot] += numFilledDrawers[aRoot];

            return bRoot;
        }

        // Else, place B under A.
        parents[bRoot] = aRoot;
        sizeOfSet[aRoot] += sizeOfSet[bRoot];

        // A set of size N represents N drawers. In this set, how many of the drawers
        // are filled?
        numFilledDrawers[aRoot] += numFilledDrawers[bRoot];

        // If rank[A] == rank[B], the rank of A increases by one.
        if (rank[aRoot] == rank[bRoot]) {
            ++rank[aRoot];
        }

        return aRoot;
    }

    // Try to store an item into a drawer.
    public boolean storeItem(int a, int b) {
        int set = unionSet(a, b);

        // If not all the drawers in the set are filled, it means that we can still
        // shift items around and put in the new item.
        if (numFilledDrawers[set] < sizeOfSet[set]) {
            ++numFilledDrawers[set];
            return true;
        }

        // If all drawers in the set are filled, it is impossible to add any new item.
        return false;
    }
}

public class Asn3A {
    public static void main(String _args[]) throws IOException {
        // Initialise IO.
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter printWriter = new PrintWriter(System.out);

        // Read N & L.
        String firstLine[] = bufferedReader.readLine().split(" ");
        int n = Integer.parseInt(firstLine[0]);
        int l = Integer.parseInt(firstLine[1]);

        // Run simulation.
        UFDS ufds = new UFDS(l);
        for (int i = 0; i < n; ++i) {
            String line[] = bufferedReader.readLine().split(" ");
            int a = Integer.parseInt(line[0]) - 1;
            int b = Integer.parseInt(line[1]) - 1;
            printWriter.println(ufds.storeItem(a, b) ? "LADICA" : "SMECE");
        }

        // Close IO.
        bufferedReader.close();
        printWriter.close();
    }
}