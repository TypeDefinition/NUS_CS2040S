// Lim Ngian Xin Terry
// A0218430N

import java.util.*;
import java.io.*;

public class Lab05 {
    public static void main(String[] args) throws IOException {
        // Create BufferedReader
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        // Read Input
        int numFrosh = Integer.parseInt(bufferedReader.readLine());
        HashMap<BitSet, Integer> combinations = new HashMap<BitSet, Integer>();
        for (int i = 0; i < numFrosh; ++i) {
            StringTokenizer tokenizer = new StringTokenizer(bufferedReader.readLine(), " ");
            int a = Integer.parseInt(tokenizer.nextToken()) - 100;
            int b = Integer.parseInt(tokenizer.nextToken()) - 100;
            int c = Integer.parseInt(tokenizer.nextToken()) - 100;
            int d = Integer.parseInt(tokenizer.nextToken()) - 100;
            int e = Integer.parseInt(tokenizer.nextToken()) - 100;

            // Store course combinations as a bitset.
            BitSet key = new BitSet(400);
            key.set(a);
            key.set(b);
            key.set(c);
            key.set(d);
            key.set(e);

            // Counter for how many people taking the combination.
            Integer value = combinations.get(key);
            if (value != null) {
                combinations.replace(key, ++value);
            } else {
                combinations.put(key, 1);
            }
        }

        // Close BufferedReader
        bufferedReader.close();

        // Sort Values
        Integer[] values = combinations.values().toArray(new Integer[0]);
        Arrays.sort(values, (Integer x, Integer y) -> { return y - x; });

        // Aggregate result.
        int result = values[0];
        for (int i = 1; i < values.length; ++i) {
            if (values[i] != values[0]) {
                break;
                
            }
            result += values[0];
        }
        
        System.out.println(String.valueOf(result));
    }
}