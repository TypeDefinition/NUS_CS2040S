// Lim Ngian Xin Terry
// A0218430N

import java.util.*;

public class Lab01 {
    private static Boolean scanMenu(String[] _items) {
        Boolean peaSoup = false;
        Boolean pancakes = false;
        for (int i = 0; i < _items.length; ++i) {
            if (_items[i].equals("pancakes")) {
                pancakes = true;
                continue;
            }
            if (_items[i].equals("pea soup")) {
                peaSoup = true;
            }
        }
        return pancakes && peaSoup;
    }

    public static void main(String[] _args) {   
        Scanner scanner = new Scanner(System.in);
        int numRestaurants = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < numRestaurants; ++i) {
            int numItems = scanner.nextInt();
            scanner.nextLine();
            String name = scanner.nextLine();
            String[] items = new String[numItems];
            for (int j = 0; j < numItems; ++j) {
                items[j] = scanner.nextLine();
            }

            if (scanMenu(items)) {
                System.out.println(name);
                scanner.close();
                return;
            }
        }

        System.out.println("Anywhere is fine I guess");
        scanner.close();
    }
}