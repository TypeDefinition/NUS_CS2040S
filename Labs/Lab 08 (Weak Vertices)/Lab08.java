// Lim Ngian Xin Terry
// A0218430N

import java.util.*;
import java.io.*;

public class Lab08 {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter printWriter = new PrintWriter(System.out);

        int n = 0;
        while ((n = Integer.parseInt(bufferedReader.readLine())) != -1) {
            int matrix[][] = new int[n][n];
            for (int row = 0; row < n; ++row) {
                String[] line = bufferedReader.readLine().split(" ");
                for (int col = row + 1; col < n; ++col) {
                    int x = Integer.parseInt(line[col]);
                    matrix[row][col] = x;
                    matrix[col][row] = x;
                }
            }

            TreeSet<Integer> weakVertices = new TreeSet<>();
            for (int row = 0; row < n; ++row) {
                boolean weak = true;
                ArrayList<Integer> neighbours = new ArrayList<>();

                // Find neighbours.
                for (int col = 0; col < n; ++col) {
                    if (matrix[row][col] == 1) {
                        neighbours.add(col);
                    }
                }

                // Check if any 2 neighbours are also connected to each other.
                for (int x = 0; x < neighbours.size() && weak; ++x) {
                    int j = neighbours.get(x);
                    for (int y = x + 1; y < neighbours.size(); ++y) {
                        int k = neighbours.get(y);
                        // If the 2 neighbours are also connected, a triangle is formed and this vertex is strong.
                        if (matrix[j][k] == 1 && matrix[k][j] == 1) {
                            weak = false;
                            break;
                        }
                    }
                }

                if (weak) { weakVertices.add(row); }
            }

            // Print result.
            if (!weakVertices.isEmpty()) {
                for (Integer i : weakVertices) { printWriter.print(i + " "); }
                printWriter.println();
            }
        }

        bufferedReader.close();
        printWriter.close();
    }
}