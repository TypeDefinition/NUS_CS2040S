// Lim Ngian Xin Terry
// A0218430N

import java.util.*;

enum HandState {
    FOLDED,
    FIST,
    PALM_DOWN,
}

class Hand {
    private final int playerNumber;
    private HandState state;

    public Hand(int playerNumber) {
        this.playerNumber = playerNumber;
        this.state = HandState.FOLDED;
    }

    public int getPlayerNumber() {
        return playerNumber;
    }

    public void advanceState() {
        if (state != HandState.PALM_DOWN) {
            state = HandState.values()[state.ordinal() + 1];
        }
    }

    public HandState getState() {
        return state;
    }
}

public class Lab04 {
    public static void main(String[] args) {
        // Read Input
        Scanner scanner = new Scanner(System.in);
        int numSyllables = scanner.nextInt();
        int numPlayers = scanner.nextInt();
        scanner.close();

        // Create Hands
        LinkedList<Hand> hands = new LinkedList<Hand>();
        for (int i = 0; i < numPlayers; ++i) {
            hands.addLast(new Hand(i+1));
            hands.addLast(new Hand(i+1));
        }

        // Simulate Game
        while (hands.size() > 1) {
            // Round Robin Touching
            int remainingSyllables = numSyllables;
            while (remainingSyllables-- > 1) {
                Hand hand = hands.removeFirst();
                hands.addLast(hand);
                if (hand.getState() == HandState.FOLDED) {
                    hands.addLast(hands.removeFirst());
                }
            }

            // Last Touched Hand(s)
            Hand lastTouchedHand = hands.removeFirst();
            switch (lastTouchedHand.getState()) {
                case FOLDED:
                // If this hand is folded, advance the state of the other hand. 
                    hands.peekFirst().advanceState();
                    lastTouchedHand.advanceState();
                    hands.addFirst(lastTouchedHand);
                    break;
                case PALM_DOWN:
                    break;
                default:
                    lastTouchedHand.advanceState();
                    hands.addLast(lastTouchedHand);
                    break;
            }
        }

        Hand winnerHand = hands.getFirst();
        System.out.println(Integer.toString(winnerHand.getPlayerNumber()));
    }
}