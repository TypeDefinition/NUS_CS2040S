// Lim Ngian Xin Terry
// A0218430N

import java.util.*;
import java.io.*;

class Vec2 {
    public final double x, y;

    public Vec2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vec2 add(Vec2 other) {
        return new Vec2(x + other.x, y + other.y);
    }

    public Vec2 sub(Vec2 other) {
        return new Vec2(x - other.x, y - other.y);
    }

    public Vec2 mult(double scalar) {
        return new Vec2(x * scalar, y * scalar);
    }

    public double dot(Vec2 other) {
        return x * other.x + y * other.y;
    }

    public double lengthSquared() {
        return x * x + y * y;
    }

    public double length() {
        return Math.sqrt(lengthSquared());
    }
}

public class Lab11 {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter printWriter = new PrintWriter(System.out);
        String line[] = null;

        final double runSpeed = 5.0;
        final double launchDistance = 50.0;
        final double launchDelay = 2.0;

        // Start
        line = bufferedReader.readLine().split(" ");
        Vec2 start = new Vec2(Double.parseDouble(line[0]), Double.parseDouble(line[1]));

        // End
        line = bufferedReader.readLine().split(" ");
        Vec2 end = new Vec2(Double.parseDouble(line[0]), Double.parseDouble(line[1]));

        // Number of Cannon
        line = bufferedReader.readLine().split(" ");
        int numCannon = Integer.parseInt(line[0]);

        // Number of Vertices = Start + Number of Cannon + End
        int numNodes = numCannon + 2;
        Vec2 nodes[] = new Vec2[numNodes];
        nodes[0] = start;
        nodes[numNodes - 1] = end;
        for (int i = 1; i < numNodes - 1; ++i) {
            line = bufferedReader.readLine().split(" ");
            nodes[i] = new Vec2(Double.parseDouble(line[0]), Double.parseDouble(line[1]));
        }

        double shortestTime[][] = new double[numNodes][numNodes];
        for (int i = 0; i < numNodes; ++i) {
            Arrays.fill(shortestTime[i], Double.POSITIVE_INFINITY);
            shortestTime[i][i] = 0.0;
        }

        // We aren't given the "edges" to this graph, so we need to calculate them ourselves.
        // Running time from start to everywhere else.
        for (int i = 1; i < numNodes; ++i) {
            shortestTime[0][i] = nodes[i].sub(nodes[0]).length() / runSpeed;
        }

        // Time from cannon to every other point excluding the start.
        for (int i = 1; i < numNodes - 1; ++i) {
            for (int j = 1; j < numNodes; ++j) {
                if (i == j) { continue; }
                double distance = nodes[j].sub(nodes[i]).length();
                double runTime = distance / runSpeed; // Time taken purely by running.
                double cannonTime = launchDelay + (Math.abs(distance - launchDistance) / runSpeed); // Time taken by cannon + running.
                shortestTime[i][j] = Math.min(runTime, cannonTime);
            }
        }

        // Floyd Warshall mah homie's algo. Other SSSP algorithms should also work? But this shit's like only 4 lines.
        for (int k = 0; k < numNodes; ++k) {
            for (int i = 0; i < numNodes; ++i) {
                for (int j = 0; j < numNodes; ++j) {
                    shortestTime[i][j] = Math.min(shortestTime[i][j], shortestTime[i][k] + shortestTime[k][j]);
                }
            }
        }

        // Print result.
        printWriter.println(shortestTime[0][numNodes - 1]);

        bufferedReader.close();
        printWriter.close();
    }
}