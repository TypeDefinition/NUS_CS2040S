// Lim Ngian Xin Terry
// A0218430N

import java.util.*;

public class DialInput {
    public char number;
    public int presses;
    public DialInput(char number, int presses) {
        this.number = number;
        this.presses = presses;
    }
}