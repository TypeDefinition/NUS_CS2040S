// Lim Ngian Xin Terry
// A0218430N

import java.util.*;
import java.io.*;

class Quest {
    private static int currentId = 0;

    public final int energy;
    public final int gold;
    public final int id;

    public Quest(int energy, int gold) {
        this.energy = energy;
        this.gold = gold;
        this.id = currentId++;
    }
}

public class Lab07 {
    public static void main(String[] args) throws Exception {
        // Create BufferedReader
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        
        // Quest BST
        TreeSet<Quest> quests = new TreeSet<>((a, b) -> {
            int e = a.energy - b.energy;
            int g = a.gold - b.gold;
            return (e == 0) ? ((g == 0) ? (a.id - b.id) : g) : e;
        });

        // Read Input
        int numCommands = Integer.parseInt(bufferedReader.readLine());
        for (int i = 0; i < numCommands; ++i) {
            String[] line = bufferedReader.readLine().split(" ");

            // Add
            if (line[0].equals("add")) {
                quests.add(new Quest(Integer.parseInt(line[1]), Integer.parseInt(line[2])));
            }
            
            // Query
            else if (line[0].equals("query")) {
                int x = Integer.parseInt(line[1]);
                long g = 0;

                // Do Quests
                Quest q = quests.floor(new Quest(x, Integer.MAX_VALUE));
                while (q != null) {
                    x -= q.energy;
                    g += (long)q.gold;
                    quests.remove(q);
                    q = quests.floor(new Quest(x, Integer.MAX_VALUE));
                }

                // Print Result
                System.out.println(Long.toString(g));
            }
            
            // Invalid Command
            else {
                throw new Exception("Invalid command.\n");
            }
        }

        // Close BufferedReader.
        bufferedReader.close();
    }
}