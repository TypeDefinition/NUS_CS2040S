// Lim Ngian Xin Terry
// A0218430N

import java.util.*;
import java.io.*;

class UFDS {
    private final int numElements;
    private final int rank[];
    private final int parents[];

    public UFDS(int numElements) {
        this.numElements = numElements;
        this.rank = new int[numElements];
        this.parents = new int[numElements];
        for (int i = 0; i < numElements; ++i) {
            this.parents[i] = i;
        }
    }

    public int getNumElements() {
        return numElements;
    }

    public int findSet(int index) {
        return (parents[index] == index) ? index : (parents[index] = findSet(parents[index]));
    }

    public boolean isSameSet(int a, int b) {
        return findSet(a) == findSet(b);
    }

    // Union set A and set B and return the root.
    public int unionSet(int a, int b) {
        // Find the set that A and B belongs to.
        int aRoot = findSet(a);
        int bRoot = findSet(b);

        // Do nothing if both elements are already in the same set.
        if (aRoot == bRoot) {
            return aRoot;
        }

        // If rank[A] < rank[B], place A under B.
        if (rank[aRoot] < rank[bRoot]) {
            parents[aRoot] = bRoot;
            return bRoot;
        }

        // Else, place B under A.
        parents[bRoot] = aRoot;

        // If rank[A] == rank[B], the rank of A increases by one.
        if (rank[aRoot] == rank[bRoot]) {
            ++rank[aRoot];
        }

        return aRoot;
    }
}

class Edge {
    public final int src, dest, dist;

    public Edge(int src, int dest, int dist) {
        this.src = src;
        this.dest = dest;
        this.dist = dist;
    }

    @Override
    public String toString() {
        return Integer.toString(src) + " " + Integer.toString(dest);
    }
}

public class Lab10 {
    // Since the minimum number of roads were constructed, the roads and Nodes
    // must've formed a tree, and the shortest edge between 2 Nodes must be the
    // direct path. Use Kruskal's algorithm.
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter printWriter = new PrintWriter(System.out);

        int n = Integer.parseInt(bufferedReader.readLine().split(" ")[0]);

        // Get edges.
        ArrayList<Edge> edges = new ArrayList<>();
        for (int src = 0; src < n; ++src) {
            String line[] = bufferedReader.readLine().split(" ");
            for (int dest = src + 1; dest < n; ++dest) {
                edges.add(new Edge(1 + src, 1 + dest, Integer.parseInt(line[dest])));
            }
        }

        // Sort edges.
        Collections.sort(edges, (x, y) -> {
            return x.dist - y.dist;
        });

        // Filter valid edges.
        UFDS ufds = new UFDS(1 + n); // Use 1 + n because our vertices index starts from 1.
        int validCounter = 0;
        for (Edge edge : edges) {
            if (ufds.isSameSet(edge.src, edge.dest)) {
                continue;
            }

            ufds.unionSet(edge.src, edge.dest);
            printWriter.println(edge.toString()); // Print results.
            if (++validCounter == n - 1) { // A tree with n vertices should have exactly n - 1 edges.
                break;
            }
        }

        bufferedReader.close();
        printWriter.close();
    }
}