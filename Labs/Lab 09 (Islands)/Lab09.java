// Lim Ngian Xin Terry
// A0218430N

import java.util.*;
import java.io.*;

enum SquareType {
    WATER, LAND, CLOUDS,
}

class GridNode {
    public final int row, col;
    public final SquareType type;
    public boolean visited;

    GridNode(int row, int col, SquareType type) {
        this.row = row;
        this.col = col;
        this.type = type;
        this.visited = false;
    }
}

public class Lab09 {
    public static void linkNodes(int r, int c, GridNode matrix[], GridNode startNode) {
        ArrayList<GridNode> queue = new ArrayList<>();
        queue.add(startNode);
        startNode.visited = true;

        for (int i = 0; i < queue.size(); ++i) {
            int row = queue.get(i).row;
            int col = queue.get(i).col;

            // Add any neighbour nodes that are land or clouds into the queue. We assume clouds are land.
            left: {
                int neighbour_row = row;
                int neighbour_col = col - 1;
                if (neighbour_row < 0 || neighbour_col < 0 || neighbour_row >= r || neighbour_col >= c) {
                    break left;
                }

                GridNode node = matrix[neighbour_row * c + neighbour_col];
                if (node.type == SquareType.WATER || node.visited) {
                    break left;
                }

                queue.add(node);
                node.visited = true;
            }

            right: {
                int neighbour_row = row;
                int neighbour_col = col + 1;
                if (neighbour_row < 0 || neighbour_col < 0 || neighbour_row >= r || neighbour_col >= c) {
                    break right;
                }

                GridNode node = matrix[neighbour_row * c + neighbour_col];
                if (node.type == SquareType.WATER || node.visited) {
                    break right;
                }

                queue.add(node);
                node.visited = true;
            }

            up: {
                int neighbour_row = row - 1;
                int neighbour_col = col;
                if (neighbour_row < 0 || neighbour_col < 0 || neighbour_row >= r || neighbour_col >= c) {
                    break up;
                }

                GridNode node = matrix[neighbour_row * c + neighbour_col];
                if (node.type == SquareType.WATER || node.visited) {
                    break up;
                }

                queue.add(node);
                node.visited = true;
            }

            down: {
                int neighbour_row = row + 1;
                int neighbour_col = col;
                if (neighbour_row < 0 || neighbour_col < 0 || neighbour_row >= r || neighbour_col >= c) {
                    break down;
                }

                GridNode node = matrix[neighbour_row * c + neighbour_col];
                if (node.type == SquareType.WATER || node.visited) {
                    break down;
                }

                queue.add(node);
                node.visited = true;
            }
        }
    }

    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter printWriter = new PrintWriter(System.out);

        String firstLine[] = bufferedReader.readLine().split(" ");
        int r = Integer.parseInt(firstLine[0]);
        int c = Integer.parseInt(firstLine[1]);

        // Fill up the matrix.
        GridNode matrix[] = new GridNode[r * c];
        for (int row = 0; row < r; ++row) {
            char[] lineBuffer = bufferedReader.readLine().split(" ")[0].toCharArray();
            for (int col = 0; col < c; ++col) {
                switch (lineBuffer[col]) {
                case 'W':
                    matrix[row * c + col] = new GridNode(row, col, SquareType.WATER);
                    break;
                case 'L':
                    matrix[row * c + col] = new GridNode(row, col, SquareType.LAND);
                    break;
                case 'C':
                    matrix[row * c + col] = new GridNode(row, col, SquareType.CLOUDS);
                    break;
                default:
                    break;
                }
            }
        }

        // Find any unvisited node that is land.
        int numIslands = 0;
        for (int row = 0; row < r; ++row) {
            for (int col = 0; col < c; ++col) {
                GridNode currNode = matrix[row * c + col];
                if (currNode.visited || currNode.type != SquareType.LAND) {
                    continue;
                }
                ++numIslands;
                // Set all the nodes that are land or clouds and linked to this node as visited.
                linkNodes(r, c, matrix, currNode);
            }
        }

        // Print answer.
        printWriter.println(numIslands);

        bufferedReader.close();
        printWriter.close();
    }
}