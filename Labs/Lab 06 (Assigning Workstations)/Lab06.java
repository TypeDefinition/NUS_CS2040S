// Lim Ngian Xin Terry
// A0218430N

import java.util.*;
import java.io.*;

class Researcher {
    public final int begin;
    public final int finish;
    
    public Researcher(int begin, int finish) {
        this.begin = begin;
        this.finish = finish;
    }
}

class WorkStation {
    public final int availStart;
    public final int availEnd;
    
    public WorkStation(int availStart, int availEnd) {
        this.availStart = availStart;
        this.availEnd = availEnd;
    }

    public Boolean canUse(Researcher r) {
        return r.begin >= availStart && r.begin <= availEnd;
    }
}

public class Lab06 {
    public static void main(String[] args) throws IOException {
        // Create BufferedReader
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        
        // Read Input
        StringTokenizer tokenizer = new StringTokenizer(bufferedReader.readLine(), " ");
        int n = Integer.parseInt(tokenizer.nextToken());
        int m = Integer.parseInt(tokenizer.nextToken());
        PriorityQueue<Researcher> researchers = new PriorityQueue<>((x, y) -> { return x.begin - y.begin; });
        for (int i = 0; i < n; ++i) {
            tokenizer = new StringTokenizer(bufferedReader.readLine(), " ");
            int a = Integer.parseInt(tokenizer.nextToken());
            int s = Integer.parseInt(tokenizer.nextToken());
            researchers.add(new Researcher(a, a + s));
        }

        int numUnlocks = 0;
        PriorityQueue<WorkStation> workStations = new PriorityQueue<>((x, y) -> { return x.availStart - y.availStart; });
        while (!researchers.isEmpty()) {
            // Get earliest researcher.
            Researcher r = researchers.poll();
            
            // Clear Invalid Workstations
            WorkStation w = workStations.peek();
            while (w != null && w.availEnd < r.begin) {
                workStations.poll();
                w = workStations.peek();
            }

            // If there is a workstation we can use, we do not need to unlock.
            if (w != null && w.canUse(r)) {
                workStations.poll();
            } else {
                ++numUnlocks;
            }

            // Update workstations.
            workStations.add(new WorkStation(r.finish, r.finish + m));
        }

        // Close BufferedReader.
        bufferedReader.close();
        
        // Print result.
        System.out.println(String.valueOf(n - numUnlocks));
    }
}